/*
 * udm.c
 *
 *  Created on: Oct 20, 2021
 *      Author: fiamma
 */


/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "udm.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define sign(x) 		(x == 0 ? 0 : (x > 0 ? 1 : -1))
//#define adc_conv(x,y) 	__HAL_ADC_CALC_DATA_TO_VOLTAGE(y, x, ADC_RESOLUTION_12B)*1e-3
#define adc_norm(x) 	(x/4096.0)					//   (adc/2^12)
#define adc_curr(x,y) 	(((adc_norm(x)*y)/0.01)/20)	//((((adc/2^12)*Vref)/Rshunt)/Gain)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
PMSM_DP_t pmsm = {
		.Vs = 26.5,
		.J = 0.000349,
		.R = 0.56,
		.L = 0.00008,
		.Kt = 13.7e-3,
		.Kf = 0.0001,
		.Tmax = 0.05,
		.Imax = 3.0,
		.vref = 3.3
};
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
static inline int32_t float2fixed(float* in);
static inline float fixed2float(int32_t* in);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
static inline int32_t float2fixed(float* in)
{
	uint32_t e = 0x4f000000;
	float* p = (float*)&e;
	float res = (*in)*(*p);

	return (int32_t)res;
}

static inline float fixed2float(int32_t* in)
{
	uint32_t e = 0x30000000;
	float* p = (float*)&e;
	float res = (*in)*(*p);

	return res;
}

/**
  * @brief  UDM initialization
  * @retval int
  */
void udm_init(HRTIM_HandleTypeDef *hrtim, ADC_HandleTypeDef *hadc[UDM_PHASE_ALL])
{
	/* Calculate PWM period */
	uint64_t fpwm = ((uint64_t)HRTIM_INPUT_CLOCK << (5 - __HAL_HRTIM_GETCLOCKPRESCALER(hrtim, HRTIM_TIMERINDEX_TIMER_B))) / PWM_PERIOD;
	pmsm.tpwm = 1.0 / (float)fpwm;
	pmsm.Kpwm = (PWM_HPERIOD*pmsm.L) / (pmsm.tpwm*pmsm.R*pmsm.Vs);

	/* Set PID regulators coefficients */
	pmsm.w_pid.Kp = pmsm.J/(pmsm.L/pmsm.R);
	pmsm.w_pid.Ki = pmsm.tpwm*pmsm.J/(pmsm.L/pmsm.R);
	pmsm.t_pid.Kp = pmsm.R;
	pmsm.t_pid.Ki = pmsm.tpwm*pmsm.R*pmsm.R/pmsm.L;

	/* PMSM PWM registers init */
	pmsm.pwma_sval = &hrtim->Instance->sTimerxRegs[0].CMP1xR; //pwma set
	pmsm.pwma_rval = &hrtim->Instance->sTimerxRegs[0].CMP3xR; //pwma reset
	pmsm.pwmb_sval = &hrtim->Instance->sTimerxRegs[4].CMP1xR; //pwmb set
	pmsm.pwmb_rval = &hrtim->Instance->sTimerxRegs[4].CMP3xR; //pwmb reset
	pmsm.pwmc_sval = &hrtim->Instance->sTimerxRegs[3].CMP1xR; //pwmc set
	pmsm.pwmc_rval = &hrtim->Instance->sTimerxRegs[3].CMP3xR; //pwmc reset

	/* Enable ADC channels */
	HAL_ADC_Start_DMA(hadc[UDM_PHASE_U], (uint32_t*)&(pmsm.adc_buf[UDM_PHASE_U][0]), 2);
	HAL_ADC_Start_DMA(hadc[UDM_PHASE_V], (uint32_t*)&(pmsm.adc_buf[UDM_PHASE_V][0]), 2);
	HAL_ADC_Start_DMA(hadc[UDM_PHASE_W], (uint32_t*)&(pmsm.adc_buf[UDM_PHASE_W][0]), 2);
}

/**
  * @brief  UDM start PWM
  * @retval int
  */
void udm_start(HRTIM_HandleTypeDef *hrtim)
{
	/* Enable HRTIM's outputs TA1, TD1 and TE1 and start timers A, D and E*/
	HAL_HRTIM_WaveformOutputStart(hrtim, HRTIM_OUTPUT_TA1 | HRTIM_OUTPUT_TA2 |
										 HRTIM_OUTPUT_TD1 | HRTIM_OUTPUT_TD2 |
										 HRTIM_OUTPUT_TE1 | HRTIM_OUTPUT_TE2);
	/* Enable Main timer controlling output timers' phases */
	HAL_HRTIM_WaveformCounterStart_IT(hrtim, HRTIM_TIMERID_TIMER_B);
	/* Enable Output timers */
	HAL_HRTIM_WaveformCounterStart(hrtim, HRTIM_TIMERID_TIMER_A |
										   HRTIM_TIMERID_TIMER_D |
										   HRTIM_TIMERID_TIMER_E);
	/* Enable Master timer controlling the main cycle */
	HAL_HRTIM_WaveformCountStart_IT(hrtim, HRTIM_TIMERID_MASTER);
}

/**
  * @brief  UDM calculate phase values
  * @retval int
  */
void udm_exec(CORDIC_HandleTypeDef *hcord)
{
	// Prepare buffers
	int32_t ibuf[6] = { 0, 0, 0, 0, 0, 0 };
	int32_t obuf[6] = { 0, 0, 0, 0, 0, 0 };
	float  scbuf[6] = { 0, 0, 0, 0, 0, 0 };

	// Convert ADC Hall sensor raw values
	pmsm.h[UDM_PHASE_U] = adc_norm(pmsm.adc_buf[UDM_PHASE_U][0]);
	pmsm.h[UDM_PHASE_V] = adc_norm(pmsm.adc_buf[UDM_PHASE_V][0]);
	pmsm.h[UDM_PHASE_W] = adc_norm(pmsm.adc_buf[UDM_PHASE_W][0]);

	// Calculate alpha and beta values
	float alfa = (0.666666667*pmsm.h[UDM_PHASE_U]) - (0.333333334*pmsm.h[UDM_PHASE_V]) - (0.333333334*pmsm.h[UDM_PHASE_W]);
	float beta = (0.577350269*pmsm.h[UDM_PHASE_V]) - (0.577350269*pmsm.h[UDM_PHASE_W]);
	// Convert to fixed point
	ibuf[0] = float2fixed(&alfa);
	ibuf[1] = float2fixed(&beta);
	// Configure CORDIC for phase calculation
	MODIFY_REG(hcord->Instance->CSR,  \
			   CORDIC_CSR_FUNC | CORDIC_CSR_SCALE,        \
			   CORDIC_FUNCTION_PHASE | CORDIC_SCALE_0);
	// Execute CORDIC core
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[0]);
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[1]);
	while (HAL_IS_BIT_CLR(hcord->Instance->CSR, CORDIC_CSR_RRDY));
	obuf[0] = (int32_t)READ_REG(hcord->Instance->RDATA);
	obuf[1] = (int32_t)READ_REG(hcord->Instance->RDATA);
	// Save the result
	pmsm.theta = obuf[0];

	// Calculate rotor velocity
	int32_t dtheta = pmsm.theta - pmsm.ptheta;
	pmsm.omega = fixed2float(&dtheta);
	// Calculate rotor acceleration
	pmsm.alpha = pmsm.omega - pmsm.pomega;
	// Save previous theta & omega values
	pmsm.ptheta = pmsm.theta;
	pmsm.pomega = pmsm.omega;
	// Speed regulator cycle
	if (pmsm.ctrl == UDM_CTRL_SPEED) {
		// Apply Omega PID controller
		pmsm.w_pid.err = pmsm.Wctrl - pmsm.omega;
		pmsm.w_pid.itg = pmsm.w_pid.itg >  pmsm.Tmax ?  pmsm.Tmax :
						 pmsm.w_pid.itg < -pmsm.Tmax ? -pmsm.Tmax :
						 pmsm.w_pid.itg + (pmsm.w_pid.Ki*pmsm.w_pid.err);
		pmsm.w_pid.out = (pmsm.w_pid.Kp*pmsm.w_pid.err) + pmsm.w_pid.itg;
		pmsm.Tctrl = pmsm.w_pid.out >  pmsm.Tmax ?  pmsm.Tmax :
					 pmsm.w_pid.out < -pmsm.Tmax ? -pmsm.Tmax :
					 pmsm.w_pid.out;
	}

	// Torque regulator routine
	pmsm.t_pid.err = pmsm.Tctrl - (pmsm.J*pmsm.alpha);
	pmsm.t_pid.itg = pmsm.t_pid.itg >  pmsm.Imax ?  pmsm.Imax :
					 pmsm.t_pid.itg < -pmsm.Imax ? -pmsm.Imax :
					 pmsm.t_pid.itg + (pmsm.t_pid.Ki*pmsm.t_pid.err);
	pmsm.t_pid.out = (pmsm.t_pid.Kp*pmsm.t_pid.err) + pmsm.t_pid.itg;
	pmsm.Ictrl = pmsm.t_pid.out >  pmsm.Imax ?  pmsm.Imax :
				 pmsm.t_pid.out < -pmsm.Imax ? -pmsm.Imax :
				 pmsm.t_pid.out;

	// Prepare input values
	ibuf[0] = pmsm.theta+0x3fffffff;	ibuf[1] = 0x7fffffff;
	ibuf[2] = pmsm.theta+0x15555555; 	ibuf[3] = 0x7fffffff;
	ibuf[4] = pmsm.theta+0x6aaaaaaa; 	ibuf[5] = 0x7fffffff;
	// Configure CORDIC core for sine calculation
	MODIFY_REG(hcord->Instance->CSR,  \
			   CORDIC_CSR_FUNC | CORDIC_CSR_SCALE,        \
			   CORDIC_FUNCTION_SINE | CORDIC_SCALE_0);
	// Execute CORDIC core
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[0]);
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[1]);
	while (HAL_IS_BIT_SET(hcord->Instance->CSR, CORDIC_CSR_RRDY));
	obuf[0] = (int32_t)READ_REG(hcord->Instance->RDATA);
	obuf[1] = (int32_t)READ_REG(hcord->Instance->RDATA);
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[2]);
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[3]);
	while (HAL_IS_BIT_SET(hcord->Instance->CSR, CORDIC_CSR_RRDY));
	obuf[2] = (int32_t)READ_REG(hcord->Instance->RDATA);
	obuf[3] = (int32_t)READ_REG(hcord->Instance->RDATA);
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[4]);
	WRITE_REG(hcord->Instance->WDATA, (uint32_t)ibuf[5]);
	while (HAL_IS_BIT_SET(hcord->Instance->CSR, CORDIC_CSR_RRDY));
	obuf[4] = (int32_t)READ_REG(hcord->Instance->RDATA);
	obuf[5] = (int32_t)READ_REG(hcord->Instance->RDATA);
	// Convert to float
	scbuf[0] = fixed2float(&obuf[0]);
	scbuf[1] = fixed2float(&obuf[1]);
	scbuf[2] = fixed2float(&obuf[2]);
	scbuf[3] = fixed2float(&obuf[3]);
	scbuf[4] = fixed2float(&obuf[4]);
	scbuf[5] = fixed2float(&obuf[5]);

	// Calculate the Clarke-Park transformation equation
	pmsm.i_ctrl[UDM_PHASE_U] = pmsm.Ictrl*scbuf[0];
	pmsm.i_ctrl[UDM_PHASE_V] = pmsm.Ictrl*scbuf[2];
	pmsm.i_ctrl[UDM_PHASE_W] = pmsm.Ictrl*scbuf[4];
}

/**
  * @brief  UDM set PWM values
  * @retval int
  */
//#pragma GCC optimize("O0")
void udm_pwm(void)
{
	int16_t ia = pmsm.adc_buf[UDM_PHASE_U][1] - pmsm.adc_off[UDM_PHASE_U];
	int16_t ib = pmsm.adc_buf[UDM_PHASE_V][1] - pmsm.adc_off[UDM_PHASE_V];
	int16_t ic = pmsm.adc_buf[UDM_PHASE_W][1] - pmsm.adc_off[UDM_PHASE_W];

	// Convert ADC current raw values
	pmsm.i[UDM_PHASE_U] = adc_curr(ia, pmsm.vref);
	pmsm.i[UDM_PHASE_V] = adc_curr(ib, pmsm.vref);
	pmsm.i[UDM_PHASE_W] = adc_curr(ic, pmsm.vref);

	// Calculate PWM values
	*pmsm.pwma_rval = PWM_HPERIOD;// + (int16_t)((pmsm.i_ctrl[UDM_PHASE_U] - pmsm.i[UDM_PHASE_U])*pmsm.Kpwm);
	*pmsm.pwmb_rval = PWM_HPERIOD;// + (int16_t)((pmsm.i_ctrl[UDM_PHASE_V] - pmsm.i[UDM_PHASE_V])*pmsm.Kpwm);
	*pmsm.pwmc_rval = PWM_HPERIOD;// + (int16_t)((pmsm.i_ctrl[UDM_PHASE_W] - pmsm.i[UDM_PHASE_W])*pmsm.Kpwm);
}

/**
  * @brief  UDM stop PWM
  * @retval int
  */
void udm_shut(HRTIM_HandleTypeDef *hrtim)
{
	/* Clear PMSM structure */
	memset(&pmsm.mode, 0, sizeof(PMSM_DP_t)-(8*sizeof(double)));
	/* Disable HRTIM's outputs TA1, TD1 and TE1 and start timers A, D and E*/
	HAL_HRTIM_WaveformOutputStop(hrtim, HRTIM_OUTPUT_TA1 | HRTIM_OUTPUT_TA2 |
									    HRTIM_OUTPUT_TD1 | HRTIM_OUTPUT_TD2 |
									    HRTIM_OUTPUT_TE1 | HRTIM_OUTPUT_TE2);

	/* Disable Master timer controlling the main cycle */
	HAL_HRTIM_WaveformCountStop_IT(hrtim, HRTIM_TIMERID_MASTER);
	/* Disable Main timer controlling output timers' phases */
	HAL_HRTIM_WaveformCounterStop_IT(hrtim, HRTIM_TIMERID_TIMER_B);
	/* Disable Output timers */
	HAL_HRTIM_WaveformCounterStop(hrtim, HRTIM_TIMERID_TIMER_A |
										 HRTIM_TIMERID_TIMER_D |
										 HRTIM_TIMERID_TIMER_E);
}
