/*
 * udm.h
 *
 *  Created on: Oct 20, 2021
 *      Author: fiamma
 */

#ifndef APPLICATION_USER_UDM_H_
#define APPLICATION_USER_UDM_H_

// UDM phase type
typedef enum UDM_PHASE_e {
	UDM_PHASE_U = 0,
	UDM_PHASE_V,
	UDM_PHASE_W,
	UDM_PHASE_ALL
} UDM_PHASE_t;

// UDM control type
typedef enum UDM_CTRL_e {
	UDM_CTRL_TORQUE = 0,
	UDM_CTRL_SPEED
} UDM_CTRL_t;

// UDM mode type
typedef enum UDM_MODE_e {
	UDM_MODE_IDLE = 0,
	UDM_MODE_INIT,
	UDM_MODE_WORK,
	UDM_MODE_SHUT
} UDM_MODE_t;

// PID controller structure
typedef struct PID_s {
	// Gain coefficients
	float Kp;
	float Ki;
	// Error storage
	float err;
	// Backward Euler integrator
	float itg;
	// Output
	float out;
} PID_t;

#pragma pack()
// PMSM data pool
typedef struct PMSM_DP_s {
  // Mechanical motor params
  const float Vs;
  const float J;
  const float R;
  const float L;
  const float Kt;
  const float Kf;
  const float Tmax;
  const float Imax;
  // Mode control state machine
  uint8_t mode;
  uint8_t ctrl;
  // ADC buffers
  __IO int16_t adc_buf[UDM_PHASE_ALL][2];
  // Phase current offset
  int16_t adc_off[UDM_PHASE_ALL];
  // Hall sensor data
  float h[UDM_PHASE_ALL];
  // Rotor angular position
  int32_t ptheta;
  int32_t theta;
  // Rotor angular velocity
  float pomega;
  float omega;
  // Rotor angular acceleration
  float alpha;
  // Control velocity
  float Wctrl;
  // Velocity regulator
  PID_t w_pid;
  // Control torque
  float Tctrl;
  // Torque regulator
  PID_t t_pid;
  // Measured currents
  float i[3];
  // Control currents
  float Ictrl;
  float i_ctrl[UDM_PHASE_ALL];
  // PWM set/reset values
  __IO uint32_t* pwma_sval;
  __IO uint32_t* pwma_rval;
  __IO uint32_t* pwmb_sval;
  __IO uint32_t* pwmb_rval;
  __IO uint32_t* pwmc_sval;
  __IO uint32_t* pwmc_rval;
  // PWM period
  float tpwm;
  // Corrent to PWM convertion coef
  float Kpwm;
  // ADC reference voltage in mV
  float vref;
} PMSM_DP_t;

// Declare the pmsm instance
extern PMSM_DP_t pmsm;

// Initialization routine
void udm_init(HRTIM_HandleTypeDef *hrtim, ADC_HandleTypeDef *hadc[UDM_PHASE_ALL]);
// Start routine
void udm_start(HRTIM_HandleTypeDef *hrtim);
// Phase calculation routine
void udm_exec(CORDIC_HandleTypeDef *hcord);
// Set PWM routine
void udm_pwm(void);
// Shutdown routine
void udm_shut(HRTIM_HandleTypeDef *hrtim);

#endif /* APPLICATION_USER_UDM_H_ */
