/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_HRTIM_MspPostInit(HRTIM_HandleTypeDef *hhrtim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define HRTIM_INPUT_CLOCK 170000000
#define MASTER_PERIOD 9000
#define PWM_PERIOD 34000
#define DT_CNT 22
#define MainHall_0_Pin GPIO_PIN_0
#define MainHall_0_GPIO_Port GPIOC
#define RSRVHall_0_Pin GPIO_PIN_1
#define RSRVHall_0_GPIO_Port GPIOC
#define MainHall_120_Pin GPIO_PIN_2
#define MainHall_120_GPIO_Port GPIOC
#define RSRVHall_120_Pin GPIO_PIN_3
#define RSRVHall_120_GPIO_Port GPIOC
#define Isense_PhaseA_Pin GPIO_PIN_0
#define Isense_PhaseA_GPIO_Port GPIOA
#define HWPower_Alert_Pin GPIO_PIN_4
#define HWPower_Alert_GPIO_Port GPIOA
#define Wheel_Voltage_Alert_Pin GPIO_PIN_5
#define Wheel_Voltage_Alert_GPIO_Port GPIOA
#define MainCAN_PWR_OFF_Pin GPIO_PIN_6
#define MainCAN_PWR_OFF_GPIO_Port GPIOA
#define RSRVCAN_PWR_OFF_Pin GPIO_PIN_7
#define RSRVCAN_PWR_OFF_GPIO_Port GPIOA
#define MainHall_PWR_OFF_Pin GPIO_PIN_4
#define MainHall_PWR_OFF_GPIO_Port GPIOC
#define RSRVHall_PWR_OFF_Pin GPIO_PIN_5
#define RSRVHall_PWR_OFF_GPIO_Port GPIOC
#define RSRVHall_240_Pin GPIO_PIN_0
#define RSRVHall_240_GPIO_Port GPIOB
#define MainHall_240_Pin GPIO_PIN_1
#define MainHall_240_GPIO_Port GPIOB
#define PWRSS_ON_Pin GPIO_PIN_2
#define PWRSS_ON_GPIO_Port GPIOB
#define Isense_phaseB_Pin GPIO_PIN_11
#define Isense_phaseB_GPIO_Port GPIOB
#define Isense_PhaseC_Pin GPIO_PIN_13
#define Isense_PhaseC_GPIO_Port GPIOB
#define PWML_C_Pin GPIO_PIN_14
#define PWML_C_GPIO_Port GPIOB
#define PWMH_C_Pin GPIO_PIN_15
#define PWMH_C_GPIO_Port GPIOB
#define Phase_I2C_SCL_Pin GPIO_PIN_6
#define Phase_I2C_SCL_GPIO_Port GPIOC
#define Phase_I2C_SDA_Pin GPIO_PIN_7
#define Phase_I2C_SDA_GPIO_Port GPIOC
#define PWML_B_Pin GPIO_PIN_8
#define PWML_B_GPIO_Port GPIOC
#define PWMH_B_Pin GPIO_PIN_9
#define PWMH_B_GPIO_Port GPIOC
#define PWML_A_Pin GPIO_PIN_8
#define PWML_A_GPIO_Port GPIOA
#define PWMH_A_Pin GPIO_PIN_9
#define PWMH_A_GPIO_Port GPIOA
#define MainCAN_RX_Pin GPIO_PIN_11
#define MainCAN_RX_GPIO_Port GPIOA
#define MainCAN_TX_Pin GPIO_PIN_12
#define MainCAN_TX_GPIO_Port GPIOA
#define BoardID_1_Pin GPIO_PIN_10
#define BoardID_1_GPIO_Port GPIOC
#define BoardID_2_Pin GPIO_PIN_11
#define BoardID_2_GPIO_Port GPIOC
#define BoardID_3_Pin GPIO_PIN_12
#define BoardID_3_GPIO_Port GPIOC
#define PWRSS_Status_Pin GPIO_PIN_3
#define PWRSS_Status_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define PWM_HPERIOD 17000//32500
#define DBG_PIN1_Pin GPIO_PIN_10
#define DBG_PIN2_Pin GPIO_PIN_12
#define DBG_GPIO_Port GPIOB
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
